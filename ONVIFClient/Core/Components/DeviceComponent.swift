//
//  DeviceComponent.swift
//  Greeting
//
//  Created by Quoc Cuong on 12/31/19.
//  Copyright © 2019 Tran Cuong. All rights reserved.
//

import UIKit

public class DeviceComponent: Component {
    public func componentType() -> ComponentType {
        return .ONVIFDevice
    }
    
    public func start() throws {
        debugPrint("DeviceComponent Did Start")
    }
    
    public func stop() {
        debugPrint("DeviceComponent Did Stop")
    }
    
    public var devices : Array<ONVIFDevice> = Array<ONVIFDevice>()
    
    public func getONVIFDevices() -> Array<ONVIFDevice>{
        return self.devices
    }
    
    public func getProfiles(for device: ONVIFDevice, success:@escaping((_ response : [Profile]) -> Void),failure:@escaping ((_ errorMessage: String?) -> Void)) {
        
        Engine.shared.getEventComponent()?.listenTo(with: EventName.http.did_update_device_profiles, event: Event(with: { (reply) in
            
            if let profiles = reply as? [Profile] {
                success(profiles)
            } else {
                failure("No profiles.")
            }
            
            }))
        
        let fetch = GetProfilesOperation()
        fetch.device = device
        
        fetch.addErrorEvent(event: Event(with: { (err) in
            failure("No profiles.")
        }))
        
        let parse = ParseProfilesOperation()
        
        parse.addErrorEvent(event: Event(with: { (err) in
            failure("No profiles.")
        }))
        
        let adapter = BlockOperation {
            parse.data = fetch.replyData
            parse.device = fetch.device
        }
        
        adapter.addDependency(fetch)
        parse.addDependency(adapter)
        
        Engine.shared.getOperationComponent()?.enqueue(operation: fetch)
        Engine.shared.getOperationComponent()?.operationQueue.addOperation(adapter)
        Engine.shared.getOperationComponent()?.enqueue(operation: parse)
    }
    
    public func getStream(for device: ONVIFDevice, profile: Profile, success:@escaping((_ response : StreamUri) -> Void),failure:@escaping ((_ errorMessage: String?) -> Void)) {
        
        Engine.shared.getEventComponent()?.listenTo(with: EventName.http.did_update_device_stream, event: Event(with: { (reply) in
            
            if let streamURI = reply as? StreamUri {
                success(streamURI)
            } else {
                failure("No streamURI.")
            }

        }))
        
        let fetch = GetStreamOperation()
        fetch.device = device
        fetch.profileToken = profile.token
        
        fetch.addErrorEvent(event: Event(with: { (err) in
            failure("No StreamURI.")
        }))

        let parse = ParseStreamResponseOperation()
        
        parse.addErrorEvent(event: Event(with: { (err) in
            failure("No StreamURI.")
        }))

        let adapter = BlockOperation {
            parse.data = fetch.replyData
            parse.device = fetch.device
            parse.profileToken = fetch.profileToken
        }
        
        adapter.addDependency(fetch)
        parse.addDependency(adapter)
        
        Engine.shared.getOperationComponent()?.enqueue(operation: fetch)
        Engine.shared.getOperationComponent()?.operationQueue.addOperation(adapter)
        Engine.shared.getOperationComponent()?.enqueue(operation: parse)
    }

    public func relativeMove(for device: ONVIFDevice, profile: Profile, panAddition: Float, tiltAddition: Float, zoomAddition: Float, success:@escaping((_ response : Data) -> Void),failure:@escaping ((_ errorMessage: String?) -> Void)) {
        
        let fetch = RelativeMoveOperation()
        fetch.device = device
        fetch.profileToken = profile.token
        let vector = PTZVector(pan: panAddition, tilt: tiltAddition, zoom: zoomAddition)
        fetch.ptzVector = vector
        
        fetch.addSuccessEvent(event: Event(with: { (reply) in
            if let data = reply as? Data {
                success(data)
            } else {
                failure("Fail")
            }
        }))
        
        fetch.addErrorEvent(event: Event(with: { (error) in
            if error != nil {
                print(error as Any)
            }
            
            failure("Fail")
        }))
        fetch.fire()
    }
        
    public func absoluteMove(for device: ONVIFDevice, profile: Profile, pan: Float, tilt: Float, zoom: Float, success:@escaping((_ response : Data) -> Void),failure:@escaping ((_ errorMessage: String?) -> Void)) {
        let fetch = AbsoluteMoveOperation()
        fetch.device = device
        fetch.profileToken = profile.token
        let vector = PTZVector(pan: pan, tilt: tilt, zoom: zoom)
        fetch.ptzVector = vector
        
        fetch.addSuccessEvent(event: Event(with: { (reply) in
            if let data = reply as? Data {
                success(data)
            } else {
                failure("Fail")
            }
        }))
        
        fetch.addErrorEvent(event: Event(with: { (error) in
            if error != nil {
                print(error as Any)
            }
            
            failure("Fail")
        }))
        
        fetch.fire()
    }
    
    public func continuousMove(for device: ONVIFDevice, profile: Profile, pan: Float, tilt: Float, zoom: Float, success:@escaping((_ response : Data) -> Void),failure:@escaping ((_ errorMessage: String?) -> Void)) {
        let fetch = ContinuousMoveOperation()
        fetch.device = device
        fetch.profileToken = profile.token
        let vector = PTZVector(pan: pan, tilt: tilt, zoom: zoom)
        fetch.ptzVector = vector
        
        fetch.addSuccessEvent(event: Event(with: { (reply) in
            if let data = reply as? Data {
                success(data)
            } else {
                failure("Fail")
            }
        }))
        
        fetch.addErrorEvent(event: Event(with: { (error) in
            if error != nil {
                print(error as Any)
            }
            
            failure("Fail")
        }))
        
        fetch.fire()
    }
    
    public func stopMoving(for device: ONVIFDevice, profile: Profile, stopPanTilt: Bool = true, stopZoom: Bool = true, success:@escaping((_ response : Data) -> Void),failure:@escaping ((_ errorMessage: String?) -> Void)) {
        let fetch = StopOperation()
        fetch.device = device
        fetch.profileToken = profile.token
        fetch.stopPanTilt = stopPanTilt
        fetch.stopZoom = stopZoom
        
        fetch.addSuccessEvent(event: Event(with: { (reply) in
            if let data = reply as? Data {
                success(data)
            } else {
                failure("Fail")
            }
        }))
        
        fetch.addErrorEvent(event: Event(with: { (error) in
            if error != nil {
                print(error as Any)
            }
            
            failure("Fail")
        }))
        
        fetch.fire()
    }
    
}
