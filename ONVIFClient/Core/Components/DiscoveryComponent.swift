//
//  Discovery.swift
//  WSExamplee
//
//  Created by Quoc Cuong on 11/12/19.
//  Copyright © 2019 Quoc Cuong. All rights reserved.
//

import UIKit

public class DiscoveryComponent: Component {
    
    public func componentType() -> ComponentType {
        return .WSDiscovery
    }
    
    public func start() throws {
        debugPrint("DiscoveryComponent Did Start")
    }
    
    public func stop() {
        
    }
    
    public var probeMatches : Array<ProbeMatch> = Array<ProbeMatch>()
    
    public func getProbeMatches() -> Array<ProbeMatch>{
        return self.probeMatches
    }
    
    public func scanOnvifDevices(success:@escaping((_ response : ONVIFDevice) -> Void)) {
        let op = SendProbeOperation()
        op.fire()
        
        Engine.shared.getEventComponent()?.listenTo(with: EventName.udp.udp_did_recognize_onvif_device, event: Event(with: { (data) in
            if let device = data as? ONVIFDevice {
                success(device)
            }
        }))

    }
    
    
}

