//
//  EngineExtension.swift
//  Greeting
//
//  Created by Quoc Cuong on 12/27/19.
//  Copyright © 2019 Tran Cuong. All rights reserved.
//

import Foundation
import CommonCrypto

public class ComponentRegister: Registrations {
    override public func registerComponents() {
        super.registerComponents()
//        Engine.shared.registerComponent(component: ViewComponent())
        Engine.shared.registerComponent(component: DiscoveryComponent())
        Engine.shared.registerComponent(component: UDP(delegate: UDPDelegateRegister()))
        Engine.shared.registerComponent(component: DeviceComponent())
        Engine.shared.registerComponent(component: HTTP())
    }
}

extension Engine{
//    func getViewComponent() -> ViewComponent?{
//        return Engine.shared.getComponent(type: ComponentType.View) as? ViewComponent
//    }
    
    public func getDeviceComponent() -> DeviceComponent?{
        return Engine.shared.getComponent(type: ComponentType.ONVIFDevice) as? DeviceComponent
    }
    
    public func getEventComponent() -> Events? {
        return Engine.shared.getComponent(type: .Event) as? Events
    }
    
    public func getDiscoveryComponent() -> DiscoveryComponent?{
        return Engine.shared.getComponent(type: ComponentType.WSDiscovery) as? DiscoveryComponent
    }
    
    public func getOperationComponent() -> OperationManage? {
        return Engine.shared.getComponent(type: .Operations) as? OperationManage
    }

}

class UDPDelegateRegister: OnvifUDPDelegate {
    override func registerReceiveOperation() {
        super.registerReceiveOperation()
        if let udp = Engine.shared.getComponent(type: .UDP) as? UDP {
            udp.registerReceiveOperation(operationType: PROBE_MATCH, operationClassName: ReceiveOnvifDeviceOperation.getClassName())
        }
    }
}

open class OnvifUDPDelegate: UDPDelegate {
    public init() { }
    
    open func registerReceiveOperation() {
        
    }
    
    
}

extension Data {
    
    public var sha1: String {
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA1_DIGEST_LENGTH))
        self.withUnsafeBytes { bytes in
            _ = CC_SHA1(bytes.baseAddress, CC_LONG(self.count), &digest)
        }
        return Data(digest).base64EncodedString()
        
    }
    
}


extension String {
    //: ### Base64 encoding a string
    func base64Encoded() -> String? {
        if let data = self.data(using: .utf8) {
            return data.base64EncodedString()
        }
        return nil
    }
    
    //: ### Base64 decoding a string
    func base64Decoded() -> String? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) {
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
}


