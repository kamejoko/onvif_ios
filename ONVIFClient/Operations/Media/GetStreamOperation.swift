//
//  GetStreamOperation.swift
//  Greeting
//
//  Created by Quoc Cuong on 1/16/20.
//  Copyright © 2020 Tran Cuong. All rights reserved.
//

import Foundation

class GetStreamOperation: OnvifHTTPOperation {
    
    var profileToken = ""
    
    override func buildRequest() -> Data? {
        let nonce = generateNonce()
        let date = generateDateString()
        
        let digest = generatePasswordDigest(from: self.device?.password ?? "", with: nonce, and: date)
        
        guard digest != "" else {
            return nil
        }
        
        let soap = """
        <?xml version="1.0" encoding="UTF-8"?>
        <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope">
        <s:Header>
        <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" s:mustUnderstand="1">
        <wsse:UsernameToken
        xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
        xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
        <wsse:Username>\(self.device?.username ?? "admin")</wsse:Username>
        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">\(digest)</wsse:Password>
        <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">\(nonce)</wsse:Nonce>
        <wsu:Created>\(date)</wsu:Created>
        </wsse:UsernameToken>
        </Security>
        </s:Header>
        <s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        <GetStreamUri xmlns="http://www.onvif.org/ver10/media/wsdl">
        <StreamSetup>
        <Stream xmlns="http://www.onvif.org/ver10/schema">RTP-Unicast</Stream>
        <Transport xmlns="http://www.onvif.org/ver10/schema">
        <Protocol>UDP</Protocol>
        </Transport>
        </StreamSetup>
        <ProfileToken>\(self.profileToken)</ProfileToken>
        </GetStreamUri>
        </s:Body>
        </s:Envelope>
        """
        
        let data = soap.data(using: .utf8)
        return data
        
    }
    
    override func processReply(reply: Any?, errMsg: String?, error: HttpError?) {
                if let data = reply as? Data {
        //            let str = String(data: data, encoding: .utf8)
        //            print(str)
                    sendSuccessEvent(param: data)
                } else {
                    sendErrorEvent(param: error)
                }

    }
}
