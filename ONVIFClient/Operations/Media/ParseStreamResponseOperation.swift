//
//  ParseStreamResponseOperation.swift
//  Greeting
//
//  Created by Quoc Cuong on 1/16/20.
//  Copyright © 2020 Tran Cuong. All rights reserved.
//

import Foundation

class ParseStreamResponseOperation: BaseXMLOperation {
    
    var device: ONVIFDevice?
    var profileToken = ""

    //PRIVATE
    private var streamUri: StreamUri?
    private var currentTag = ""
    private var currentElement = ""
    
    override func processReply(reply: Any?, type: Int, errMsg: String?) {
        if let stream = reply as? StreamUri {
            
            for i in 0..<(Engine.shared.getDeviceComponent()?.devices.count ?? 0) {
                if (Engine.shared.getDeviceComponent()?.devices[i].xAddr == device?.xAddr) {
                    for profile in Engine.shared.getDeviceComponent()?.devices[i].profiles ?? [] {
                        if profile.token == self.profileToken {
                            profile.stream = stream
                            Engine.shared.getEventComponent()?.trigger(eventName: EventName.http.did_update_device_stream, information: stream)
                            break
                        }
                    }
                }
            }

        } else {
            sendErrorEvent(param: nil)
        }
    }
    
    func parserDidStartDocument(_ parser: XMLParser) {
        self.streamUri = StreamUri()
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.currentElement = elementName
        if elementName.contains("MediaUri") {
            self.currentTag = "MediaUri"
        }
        
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName.contains("MediaUri") {
            self.currentTag = ""
        }
        //        print(elementName)
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        if self.currentTag == "MediaUri" {
            if self.currentElement.contains("Uri") {
                self.streamUri?.mediaUri.uri = string
            } else if self.currentElement.contains("InvalidAfterConnect") {
                self.streamUri?.mediaUri.invalidAfterConnect = (string.contains("true"))
            } else if self.currentElement.contains("InvalidAfterReboot") {
                self.streamUri?.mediaUri.invalidAfterReboot = (string.contains("true"))
            } else if self.currentElement.contains("Timeout") {
                self.streamUri?.mediaUri.timeout = string
            }
        }
        
        self.currentElement = ""
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        processReply(reply: self.streamUri, type: 0, errMsg: nil)
    }
}
