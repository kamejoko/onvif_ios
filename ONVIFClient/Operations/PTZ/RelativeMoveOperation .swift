//
//  RelativeMoveOperation .swift
//  Greeting
//
//  Created by Quoc Cuong on 1/16/20.
//  Copyright © 2020 Tran Cuong. All rights reserved.
//

import Foundation

class RelativeMoveOperation: OnvifHTTPOperation {
    
    var profileToken = ""
    var ptzVector: PTZVector?

    override func buildRequest() -> Data? {
        let nonce = generateNonce()
        let date = generateDateString()
        
        let digest = generatePasswordDigest(from: self.device?.password ?? "", with: nonce, and: date)
        
        guard digest != "" else {
            return nil
        }
        
        let soap = """
        <?xml version="1.0" encoding="UTF-8"?>
        <s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope"
        xmlns:tt="http://www.onvif.org/ver10/schema"
        xmlns:trt="http://www.onvif.org/ver20/ptz/wsdl">
        <s:Header>
        <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" s:mustUnderstand="1">
        <UsernameToken>
        <Username>\(self.device?.username ?? "admin")</Username>
        <Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">\(digest)</Password>
        <Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">\(nonce)</Nonce>
        <Created xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\(date)</Created>
        </UsernameToken>
        </Security>
        </s:Header>
        <s:Body>
        <trt:RelativeMove>
        <trt:ProfileToken>\(self.profileToken)</trt:ProfileToken>
        <trt:Translation>
        <tt:PanTilt x="\(self.ptzVector?.panTilt?.x ?? 0)" y="\(self.ptzVector?.panTilt?.y ?? 0)"></tt:PanTilt>
        <tt:Zoom x="\(self.ptzVector?.zoom?.x ?? 0)"></tt:Zoom>
        </trt:Translation>
        </trt:RelativeMove>
        </s:Body>
        </s:Envelope>
        """
        
//        print(soap)
        let data = soap.data(using: .utf8)
        return data
    }
    
    override func processReply(reply: Any?, errMsg: String?, error: HttpError?) {
        if let data = reply as? Data {
//            let str = String(data: data, encoding: .utf8)
//            print(str)
            sendSuccessEvent(param: data)
        } else {
            sendErrorEvent(param: error)
        }

    }
}
