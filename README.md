# ONVIF Client
ONVIF Client implementation on iOS

**Target:**
iOS, 9.0.

## **Installation**

ONVIFClient is Carthage compatible. To include it add the following line to your Cartfile

  `git "http://gitlab.vivas.vn/cuongtq/onvifclient" "master"`

The project is currently configured to build for iOS. After building with carthage the resultant frameworks will be stored in:

  `Carthage/Build/iOS/ONVIFClient.framework`

Select the framework and drag it into your project.

## **Start**

Whenever you want to start:

        Engine.shared.start(with: ComponentRegister())
        
Commonly in AppDelegate:

        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        if(mainWindow == nil){
            mainWindow = UIWindow(frame: UIScreen.main.bounds)
            if #available(iOS 13.0, *) {
                mainWindow?.overrideUserInterfaceStyle = .light
            }
            mainWindow?.rootViewController = ViewController()
            mainWindow?.makeKeyAndVisible()
            
        }
        Engine.shared.start(with: ComponentRegister())
        return true
    }


**Scan ONVIF device:**

This triggered everytime DiscoveryComponent recognized an ONVIF device over UDP.

        Engine.shared.getDiscoveryComponent()?.scanOnvifDevices(success: { (device) in
            print(device.hardware)
        })

 
    
**Get ONVIF device's profiles:**

Set username and password for device if needed. (once only)

        if let device = Engine.shared.getDeviceComponent()?.devices.first {
            device.set(username: "admin", password: "xxxxxx")
            Engine.shared.getDeviceComponent()?.getProfiles(for: device, success: { (profiles) in
                //                print(profiles)
            }, failure: { (err) in
                print(err)
            })
        }
        
**Get ONVIF device's streamURI:**

        Engine.shared.getDeviceComponent()?.getStream(for: device!, profile: profile!, success: { (streamURI) in
            print(streamURI)
        }, failure: { (err) in
            print(err)
        })


**Relative Move:**

        Engine.shared.getDeviceComponent()?.relativeMove(for: device!, profile: profile!, panAddition: 0.5, tiltAddition: 0.5, zoomAddition: 0.1, success: { (reply) in

        }, failure: { (err) in

        })

**Absolute Move:**

        Engine.shared.getDeviceComponent()?.absoluteMove(for: device!, profile: profile!, pan: 0, tilt: 0, zoom: 0, success: { (reply) in
            
        }, failure: { (err) in
            
        })
        
**Continuous Move:**

        Engine.shared.getDeviceComponent()?.continuousMove(for: device!, profile: profile!, pan: 0.1, tilt: 0.1, zoom: 0.1, success: { (reply) in
            
        }, failure: { (err) in
            
        })
        
**Stop Moving:**

Stop any device's movement

        Engine.shared.getDeviceComponent()?.stopMoving(for: device!, profile: profile!, success: { (reply) in
            
        }, failure: { (err) in
            
        })
        
Or you can stop device's specific movement with parameters: stopPanTilt, stopZoom

        Engine.shared.getDeviceComponent()?.stopMoving(for: device!, profile: profile!, stopPanTilt: true, stopZoom: false, success: { (reply) in
            
        }, failure: { (err) in
            
        })

