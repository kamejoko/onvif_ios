//
//  AppDelegate.swift
//  Sample
//
//  Created by Quoc Cuong on 1/17/20.
//  Copyright © 2020 Tran Cuong. All rights reserved.
//

import UIKit
import ONVIFClient

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var mainWindow: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        if(mainWindow == nil){
            mainWindow = UIWindow(frame: UIScreen.main.bounds)
            if #available(iOS 13.0, *) {
                mainWindow?.overrideUserInterfaceStyle = .light
            }
            mainWindow?.rootViewController = ViewController()
            mainWindow?.makeKeyAndVisible()
            
        }
        Engine.shared.start(with: ComponentRegister())
        return true
    }
}

