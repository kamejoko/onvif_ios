//
//  ViewController.swift
//  Sample
//
//  Created by Quoc Cuong on 1/17/20.
//  Copyright © 2020 Tran Cuong. All rights reserved.
//

import UIKit
import ONVIFClient

class ViewController: UIViewController {
    
    lazy var btn: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("btn", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.backgroundColor = .white
        btn.titleLabel?.textAlignment = .left
        btn.addTarget(self, action: #selector(handleBtn), for: .touchUpInside)
        self.view.addSubview(btn)
        return btn
    }()
    
    lazy var btn1: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("btn1", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        btn.backgroundColor = .white
        btn.titleLabel?.textAlignment = .left
        btn.addTarget(self, action: #selector(handleBtn1), for: .touchUpInside)
        self.view.addSubview(btn)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = .white
        
        btn.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 32).isActive = true
        btn.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 8).isActive = true
        //        btn.widthAnchor.constraint(equalToConstant: 100).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        btn1.topAnchor.constraint(equalTo: self.btn.bottomAnchor, constant: 32).isActive = true
        btn1.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 8).isActive = true
        //        btn1.widthAnchor.constraint(equalToConstant: 100).isActive = true
        btn1.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Engine.shared.getDiscoveryComponent()?.scanOnvifDevices(success: { (device) in
            print(device.hardware)
        })
        
    }
    
    @objc func handleBtn() {
        if let device = Engine.shared.getDeviceComponent()?.devices.first {
            device.set(username: "admin", password: "vivasvnpt124")
            Engine.shared.getDeviceComponent()?.getProfiles(for: device, success: { (profiles) in
                //                print(profiles)
            }, failure: { (err) in
                print(err)
            })
        }
    }
    
    @objc func handleBtn1() {
        
        let device = Engine.shared.getDeviceComponent()?.devices.first
        let profile = device!.profiles.first
        
//        Engine.shared.getDeviceComponent()?.getStream(for: device!, profile: profile!, success: { (streamURI) in
//            print(streamURI)
//        }, failure: { (err) in
//            print(err)
//        })
        
        Engine.shared.getDeviceComponent()?.absoluteMove(for: device!, profile: profile!, pan: 0, tilt: 0, zoom: 0, success: { (reply) in
            
        }, failure: { (err) in
            
        })
        Engine.shared.getDeviceComponent()?.relativeMove(for: device!, profile: profile!, panAddition: 0.5, tiltAddition: 0.5, zoomAddition: 0.1, success: { (reply) in

        }, failure: { (err) in

        })
        Engine.shared.getDeviceComponent()?.continuousMove(for: device!, profile: profile!, pan: 0.1, tilt: 0.1, zoom: 0.1, success: { (reply) in
            
        }, failure: { (err) in
            
        })
        
        Engine.shared.getDeviceComponent()?.stopMoving(for: device!, profile: profile!, stopPanTilt: true, stopZoom: false, success: { (reply) in
            
        }, failure: { (err) in
            
        })
    }
    
}

